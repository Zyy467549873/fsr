# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django.conf.urls import url

from dashboard import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^index_home', views.IndexHome.as_view(), name='index_home'),
    #url(r'^table.json', views.TableJson.as_view(), name='tablejson'),
    url(r'^init.json', views.InitJson.as_view(), name='InitJson'),
    url(r'^clear.json', views.ClearJson.as_view(), name='ClearJson'),
]