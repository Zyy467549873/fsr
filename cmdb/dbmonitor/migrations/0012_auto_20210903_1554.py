# Generated by Django 2.0.6 on 2021-09-03 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbmonitor', '0011_mysqlconfig_mysql_master_slave'),
    ]

    operations = [
        migrations.AddField(
            model_name='mysqlconfig',
            name='Slave_IO_Running',
            field=models.IntegerField(blank=True, null=True, verbose_name='io线程状态'),
        ),
        migrations.AddField(
            model_name='mysqlconfig',
            name='Slave_SQL_Running',
            field=models.IntegerField(blank=True, null=True, verbose_name='sql线程状态'),
        ),
    ]
